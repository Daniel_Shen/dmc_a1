function threahold(threahold, grayhis){
  var lessTSum=0;
  var lessTAvg=0;
  var lessSum=0;
    for(var i=0;i<=threahold;i++){
  	lessTSum=lessTSum+grayhis[i]*i;
  	lessSum+=grayhis[i];
  }
  
  lessTAvg=Math.round(lessTSum/lessSum);
  
  var greaterTSum=0;
  var greaterTAvg=0;
  var greaterSum=0;
  for(var i=threahold;i<grayhis.length;i++){
  	greaterTSum=greaterTSum+grayhis[i]*i;
  	greaterSum+=grayhis[i];
  }
  greaterTAvg=Math.round(greaterTSum/greaterSum);
  
  var newThreahold=Math.round((lessTAvg+greaterTAvg)/2);
  return newThreahold;
}

//initial()
var img;

function init() {
  
 //ctx is a global canvas.	
  	var	canvas = document.getElementById('canvas');
  	var canvasT= document.getElementById('canvasT');
  	var canvasS= document.getElementById('canvasS');
  	var canvasE= document.getElementById('canvasE');
  	var canvasSUM= document.getElementById('canvasSum');
  	
  	//var imageInput=document.getElementById('image').value;		
  	var ctx = document.getElementById('canvas').getContext('2d');
  	var ctxT = document.getElementById('canvasT').getContext('2d');
  	var ctxS = document.getElementById('canvasS').getContext('2d');
  	var ctxE = document.getElementById('canvasE').getContext('2d');
  	var ctxSUM = document.getElementById('canvasSum').getContext('2d');
  	
  	//to store rectangel position for 3 methods.
  	//make them all global because they would be used by other methods.
  	 rectT=new Array(4);
  	 rectS=new Array(4);
  	 rectE=new Array(4);
  	 rectSUM=new Array(4);
  	
	img = new Image();
	img.src = "cool.jpg";
	img.onload = function(){
		canvas.width=canvasT.width=canvasS.width=canvasE.width=canvasSUM.width=img.width;
		canvas.height=canvasT.height=canvasS.height=canvasE.height=canvasSUM.height=img.height;
		
		
		ctx.drawImage(img,0,0,canvas.width,canvas.height);
		ctxT.drawImage(img,0,0,canvas.width,canvas.height);
		ctxS.drawImage(img,0,0,canvas.width,canvas.height);
		ctxE.drawImage(img,0,0,canvas.width,canvas.height);
		ctxSUM.drawImage(img,0,0,canvas.width,canvas.height);
	}
}

function uploadImage(){
	img= new Image();
	var filename = document.getElementById('image').value; 
	drawInitial(filename);
}

function drawInitial(imageName){
	var	canvas = document.getElementById('canvas');
		var canvasT= document.getElementById('canvasT');
		var canvasS= document.getElementById('canvasS');
		var canvasE= document.getElementById('canvasE');
		var canvasSUM= document.getElementById('canvasSum');
		
		//var imageInput=document.getElementById('image').value;		
		var ctx = document.getElementById('canvas').getContext('2d');
		var ctxT = document.getElementById('canvasT').getContext('2d');
		var ctxS = document.getElementById('canvasS').getContext('2d');
		var ctxE = document.getElementById('canvasE').getContext('2d');
		var ctxSUM = document.getElementById('canvasSum').getContext('2d');
		
		//to store rectangel position for 3 methods.
		//make them all global because they would be used by other methods.
		
		img.src = imageName;
		img.onload = function(){
			canvas.width=canvasT.width=canvasS.width=canvasE.width=canvasSUM.width=img.width;
			canvas.height=canvasT.height=canvasS.height=canvasE.height=canvasSUM.height=img.height;
			
			
			ctx.drawImage(img,0,0,canvas.width,canvas.height);
			ctxT.drawImage(img,0,0,canvas.width,canvas.height);
			ctxS.drawImage(img,0,0,canvas.width,canvas.height);
			ctxE.drawImage(img,0,0,canvas.width,canvas.height);
			ctxSUM.drawImage(img,0,0,canvas.width,canvas.height);
	}
	
}

//initialize the arry with default value;
function array256(default_value) {
  arr = [];
  for (var i=0; i<256; i++) { arr[i] = default_value; }
  return arr;
}

//threahold selection
function threaholdselection(T, grayhis){	
	var newThreahold=threahold(T, grayhis);
	try{
	
		while(newThreahold!=T)
			{
				newThreahold=threahold(newThreahold, grayhis);
				T=newThreahold;
				
			}
		}catch(err)
		{
			alert("threadselect error");
		}
	return newThreahold;
   }


//convert to grayscale image

function threholding(){
try{
	 var output, w,h,inputdata,outputdata;
	 var input;
	 
	 var grayscale=array256(0);
	 var grayavg=0;

	 var canvas = document.getElementById('canvas');
	 var ctx = document.getElementById('canvas').getContext('2d');
	 var canvasT = document.getElementById('canvasT');
	 var ctxT = document.getElementById('canvasT').getContext('2d');
	 //var imageInput=document.getElementById('image').value;		
	 //ctxT = document.getElementById('canvasT').getContext('2d');
	
	 try{
	 	input = ctx.getImageData(0, 0, canvas.width, canvas.height);
	 }catch(e){
	 	netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
	 	input = ctx.getImageData(0, 0, canvas.width, canvas.height);
	  }
	 //output = ctx.createImageData(canvas.width, canvas.height);
	 
	 w=input.width;
	 h=input.height;
	
	 inputdata=input.data;
	 //outputdata=output.data;
	 
	 var garysum=0;
	 for(var i=0;i<w*h;i++){
		//get gray value;
		var gray = inputdata[4*i]*0.3+inputdata[4*i+1]*0.59+inputdata[4*i+2]*0.11;
		
		grayscale[gray]+=1;
		garysum+=gray;
	 }
	 
	 //get final threahold
	 grayavg=Math.round(garysum/(canvas.width*canvas.height));
	 var finalThreahold=threaholdselection(grayavg,grayscale);
	 
	 //filter pixel with threahold.
	 for(var i=0;i<w*h;i++){
	 	var gray = inputdata[4*i]*0.3+inputdata[4*i+1]*0.59+inputdata[4*i+2]*0.11;
	 	if(gray<finalThreahold){
	 	//backgroud=0;
			inputdata[4*i]=inputdata[4*i+1]=inputdata[4*i+2]=0;
	 	}else{
	 	//foreground=255;
	 			inputdata[4*i]=inputdata[4*i+1]=inputdata[4*i+2]=255;
	 		}
	 }
	 
	 var result=erosionImage(erosionImage(input));
	 ctxT.putImageData(result,0,0);
	 rectT=rect("red",result,canvasT);
	 //alert(rectT);
	
}
	catch(err)
	{
		var errormsg=err.description;
		alert("Error cause in threholding:\n"+errormsg);
	}
		
}
