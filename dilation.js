//erosion for binary image.
//foreground=white=255;
//background=black=0;
function dilation(x,y,filterSize,img)
{			
		//alert(img.width);
		var offset = Math.floor(filterSize / 2);
		for (var i = 0; i < filterSize; i++){
			for (var j = 0; j < filterSize; j++){
			// What pixel are we testing
			var xloc = x+i-offset;
			var yloc = y+j-offset;
			// Is it in range?
				if (xloc >= 0 && xloc <= img.width-filterSize
		 		&& yloc >= 0 && yloc <= img.height-filterSize) {
					var loc = (xloc + (img.width)*yloc)*4;
					if(img.data[loc]==0)
						{
							return 0;
						}
		  		}
			}
		}
		return 255;
}

//dilate the Image date 
function dilationImage(InputImage){
	var canvas = document.getElementsByTagName('canvas')[0];
	var ctx = document.getElementById('canvas').getContext('2d');

	var output=ctx.createImageData(canvas.width, canvas.height);
	
	for (var x = 0; x < canvas.width; x++) {
	   for (var y = 0; y < canvas.height; y++ ) {
	   		if(InputImage.data[4*(x+y*canvas.width)]==255){
	   		//find out the object pixel
	   			var c = dilation(x,y,3,InputImage);
	   			if(c==0){
	   			//this pixel can be dilated.
		   			var offset = Math.floor(filterSize / 2);
		   			var filterSize=3;
		   			for (var i = 0; i < filterSize; i++){
		   				for (var j = 0; j < filterSize; j++){
		   				// What pixel are we testing
		   				var xloc = x+i-offset;
		   				var yloc = y+j-offset;
		   				// Is it in range?
		   					if (xloc >= 0 && xloc <= InputImage.width-filterSize
		   			 		&& yloc >= 0 && yloc <= InputImage.height-filterSize) {
		   						var loc = (xloc + (InputImage.width)*yloc)*4;
		   						output.data[loc]=output.data[loc+1]=output.data[loc+2]=255;
		   						output.data[loc+3]=InputImage.data[loc+3];
		   			  		}
		   				}
		   			}
	   			}else{
	   				output.data[4*(x+y*canvas.width)]  =InputImage.data[4*(x+y*canvas.width)];
	   				output.data[4*(x+y*canvas.width)+1]=InputImage.data[4*(x+y*canvas.width)+1];
	   				output.data[4*(x+y*canvas.width)+2]=InputImage.data[4*(x+y*canvas.width)+2];
	   				output.data[4*(x+y*canvas.width)+3]=InputImage.data[4*(x+y*canvas.width)+3];
	   			}
	   		}
	   		else{
	   			output.data[4*(x+y*canvas.width)]  =InputImage.data[4*(x+y*canvas.width)];
	   			output.data[4*(x+y*canvas.width)+1]=InputImage.data[4*(x+y*canvas.width)+1];
	   			output.data[4*(x+y*canvas.width)+2]=InputImage.data[4*(x+y*canvas.width)+2];
	   			output.data[4*(x+y*canvas.width)+3]=InputImage.data[4*(x+y*canvas.width)+3];
	   		}
	     }
	}
	
	//ctx.putImageData(output,0,0);
	return output;
}