//values: colour
//inputImage: processed Image Object Data;
//canvas: the place where a retangle will be draw on.
function rect(values,inputImage,canvas) {
            var Co = canvas.getContext('2d');
 
            {
 				var  Do = inputImage;
 
                region = new Array (Do.width*Do.height);
                index = 0;
                region[0] = {};
                region[0]['weight'] = 0;
                region[0]['x1'] = 0;
                region[0]['x2'] = 0;
                region[0]['y1'] = 0;
                region[0]['y2'] = 0;
 
 				//initialize the regin array.
                for (var x = 0; x < Do.width; x++) {              // >
                  for (var y = 0; y < Do.height; y++) {           // >
                    var i = (x + Do.width*y)*4;
                    var gray = (Do.data[i+0]+Do.data[i+1]+Do.data[i+2])/3;

                    Do.data[i+0]=gray;   Do.data[i+1]=gray;   Do.data[i+2]=gray;
 
                    var j=i/4;  // Index at the pixel level, not byte level
                    region[j] = {};
                    region[j]['weight'] = (gray > 18) ? 1 : 0;
                    region[j]['x1'] = x;
                    region[j]['x2'] = x;
                    region[j]['y1'] = y;
                    region[j]['y2'] = y;
                  }
                }
             
                Co.beginPath();
                Co.strokeStyle = values;
                Co.lineWidth = "2";
 
 
 
          // segmentation
          // now grow regions around each identified pixel (weight>0)
          // add onto each pixel the neighbor's weight within 5x5 distance
          // and remember the extent of the rectangular region that these cover
          for (var x = 0; x < Do.width; x++) {              // >
            for (var y = 0; y < Do.height; y++) {           // >
              var i = x + Do.width*y;
              // for each identified pixel...
              if (region[i]['weight'] > 0) {
                // check its neighbors
                for (xn = Math.max(x-2,0); xn <= Math.min(x+2,Do.width-1); xn++) {
                  for (yn = Math.max((y-2),0); yn <= Math.min((y+2),(Do.height-1)); yn++) {
                    var j = xn + Do.width*yn;
                    if (j != i) {
                      // ...but count only other identified neighbours.
                      if (region[j]['weight'] > 0) {
                        // if 'in', then enlarge the bounding rect to include it
                        region[i]['weight'] += region[j]['weight'];
                        region[i]['x1'] = Math.min(region[i]['x1'], region[j]['x1']);
                        region[i]['y1'] = Math.min(region[i]['y1'], region[j]['y1']);
                        region[i]['x2'] = Math.max(region[i]['x2'], region[j]['x2']);
                        region[i]['y2'] = Math.max(region[i]['y2'], region[j]['y2']);
                      }
                    }
                  } // for i
                } // for i
              } // if
            } // for y
          } // for x
 
          // place a rect around one of the heaviest pixels, which should be one of the largest clusters
 
          maxWeight = 0;
          index = 0; // reset
          for (var x = 0; x < Do.width; x++) {
            for (var y = 0; y < Do.height; y++) {
              var i = x + Do.width*y;
              if (region[i]['weight'] > maxWeight) {
                maxWeight = region[i]['weight'];
                index = i;
              }
            } // for y
          } // for x
          r = region[index];
          Co.strokeRect(r['x1'],        r['y1'], 
                    r['x2']-r['x1'],    r['y2']-r['y1']);
                    
         return [r['x1'], r['y1'],  r['x2']-r['x1'],r['y2']-r['y1']];
         
        } // img.onload()
 
      }
        
        