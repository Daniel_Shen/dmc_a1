
/**
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
 0.36,0.68.
 */
 
function rgbToHsv(r, g, b){
    r = r/255, g = g/255, b = b/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, v = max;

    var d = max - min;
    s = max == 0 ? 0 : d / max;

    if(max == min){
        h = 0; // achromatic
    }else{
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, v];
}
function array360(default_value) {
  arr = [];
  for (var i=0; i<360; i++) { arr[i] = default_value; }
  return arr;
}


//return canvas modified.
function skinDectCore(InputImage){
	var canvas = document.getElementsByTagName('canvas')[0];
	var ctx = document.getElementById('canvas').getContext('2d');
	var ctxS = document.getElementById('canvasS').getContext('2d');
	var canvasS= document.getElementById('canvasS');
	var inputData =InputImage.data;
	var output=ctx.createImageData(canvas.width, canvas.height);
	var outputData=output.data;
	
	for(var i=0;i<canvas.width*canvas.height;i++){
		var R=inputData[4*i];
		var G=inputData[4*i+1];
		var B=inputData[4*i+2];
		var A=inputData[4*i+3];
		
		var hsv=rgbToHsv(R,G,B);
		var h=hsv[0];
		var s=hsv[1];
		
		if((h>=0.0 && h<=0.5)&&(s>=0.23 && s<=0.68)){
			outputData[4*i]=outputData[4*i+1]=outputData[4*i+2]=255;
		}else{
			outputData[4*i]=outputData[4*i+1]=outputData[4*i+2]=0;
		}
		outputData[4*i+3]=A;
	}
	
	var result=erosionImage(dilationImage(output));
	//used to actually change the pixel data ofcanva element.
	ctx.putImageData(result,0,0);
	return canvas;
}


function skinDect(){
	var input;
	var canvas = document.getElementsByTagName('canvas')[0];
	var ctx = document.getElementById('canvas').getContext('2d');
	var canvasS= document.getElementById('canvasS');
	var ctxS = document.getElementById('canvasS').getContext('2d');
	
	try{
		input = ctx.getImageData(0, 0, canvas.width, canvas.height);
	}catch(e){
		netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
		input = ctx.getImageData(0, 0, canvas.width, canvas.height);
	 }
	
	var inputData =input.data;
	var output=ctx.createImageData(canvas.width, canvas.height);
	var outputData=output.data;
	
	for(var i=0;i<canvas.width*canvas.height;i++){
		var R=inputData[4*i];
		var G=inputData[4*i+1];
		var B=inputData[4*i+2];
		var A=inputData[4*i+3];
		
		var hsv=rgbToHsv(R,G,B);
		var h=hsv[0];
		var s=hsv[1];
		
		if((h>=0.0 && h<=0.5)&&(s>=0.23 && s<=0.68)){
			outputData[4*i]=outputData[4*i+1]=outputData[4*i+2]=255;
		}else{
			outputData[4*i]=outputData[4*i+1]=outputData[4*i+2]=0;
		}
		outputData[4*i+3]=A;
	}
	
	var result=erosionImage(dilationImage(output));
	//var result1=dilationImage(output); 
	ctxS.putImageData(result,0,0);
	rectS=rect("green",result,canvasS);
	//alert(rectS);
}