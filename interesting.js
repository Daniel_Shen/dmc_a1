//Finding the most interesting area, that is the area where tree retangles overlaps
function interesting(){

	var canvas = document.getElementsByTagName('canvas')[0];
	var ctx = document.getElementById('canvas').getContext('2d');
	var canvasSumI = document.getElementById('canvasSum');
	var ctxSumI = document.getElementById('canvasSum').getContext('2d');
	
	//var output=ctx.createImageData(canvas.width, canvas.height);
	var sX=0;
	var sY=0;
	var eX=0;
	var eY=0;
	var first=1;
	
	
	for (var y = 0; y < canvas.height ; y++) {
	   for (var x = 0; x < canvas.width; x++ ) {
	   	 var loc=4*(x+y*canvas.width);
	     if(inRange(x,y,rectT) && inRange(x,y,rectS) && inRange(x,y,rectE)){
	     	if(first==1){
	     		sX=x;
	     		sY=y;
	     	}else{
	     		eX=x;
	     		eY=y;
	     	}
	     	first=0;
	     	
	     }else{
	     }
	   }
	}
	//ctxSum.putImageData(output,0,0);
	ctxSumI.beginPath();
	ctxSumI.strokeStyle = "Red";
	ctxSumI.lineWidth = "2";
	ctxSumI.strokeRect(sX,sY,eX-sX,eY-sY);
}

//checking if the pixel is in all 3 retangles.
function inRange(x,y,rect){
	var xr=rect[0];
	var yr=rect[1];
	var wr=rect[2];
	var hr=rect[3];

	if(x>=xr && x<=xr+wr && y>=yr&& y<=yr+hr){
		return true;
	}else{
		return false;
	}
}