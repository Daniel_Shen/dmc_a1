//erosion for binary image.
//foreground=white=255;
//background=black=0;

function erosion(x,y,filterSize,img)
{			
		//alert(img.width);
		var offset = Math.floor(filterSize / 2);
		for (var i = 0; i < filterSize; i++){
			for (var j = 0; j < filterSize; j++){
			// What pixel are we testing
			var xloc = x+i-offset;
			var yloc = y+j-offset;
			// Is it in range?
				if (xloc >= 0 && xloc <= img.width-filterSize
		 		&& yloc >= 0 && yloc <= img.height-filterSize) {
					var loc = (xloc + (img.width)*yloc)*4;
					if(img.data[loc]==0)
						{
							return 0;
						}
		  		}
			}
		}
		return 255;
}

//erosion 
//return erosed image data.
function erosionImage(InputImage){
	var canvas = document.getElementsByTagName('canvas')[0];
	var ctx = document.getElementById('canvas').getContext('2d');	 
	var output=ctx.createImageData(canvas.width, canvas.height);
	
	for (var x = 0; x < canvas.width; x++) {
	   for (var y = 0; y < canvas.height; y++ ) {
	   		if(InputImage.data[4*(x+y*canvas.width)]==255){
	   			var c = erosion(x,y,3,InputImage);
	   			output.data[4*(x+y*canvas.width)]=output.data[4*(x+y*canvas.width)+1]=output.data[4*(x+y*canvas.width)+2]=c	   			
	   			output.data[4*(x+y*canvas.width)+3]=255;
	   		}
	   		else{
	   			output.data[4*(x+y*canvas.width)]  =InputImage.data[4*(x+y*canvas.width)];
	   			output.data[4*(x+y*canvas.width)+1]=InputImage.data[4*(x+y*canvas.width)+1];
	   			output.data[4*(x+y*canvas.width)+2]=InputImage.data[4*(x+y*canvas.width)+2];
	   			output.data[4*(x+y*canvas.width)+3]=InputImage.data[4*(x+y*canvas.width)+3];
	   		}
	     }
	}
	return output;
}